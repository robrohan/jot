.PHONY: all test clean

hash = $(shell git log --pretty=format:'%h' -n 1)

build: clean
	JOT_VERSION=$(hash) npm run build
	cp LICENSE dist/LICENSE

clean:
	rm -rf dist
	rm -rf .cache

test:
	npm run test

start: clean
	npm run start

publish: build
	aws s3 sync --delete --region us-west-2 \
		--cache-control max-age=604800 \
		dist s3://jotcanvas.com
	aws cloudfront create-invalidation \
		--distribution-id E2LTBH2YEHIGRC \
		--paths "/*"
