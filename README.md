# Jot Canvas

Jot Canvas is meant to be an easy way to share whiteboards between browsers without needing a login or signup process. It also work with an iPad (and with an Apple Pencil) to allow for "casting" to a larger device. For example, you could use Jot Canvas to draw diagrams on your iPad and "cast" the session to your large display on your Linux computer.

Here is a [video of it in action](https://www.youtube.com/watch?v=v5MbsjOSWvc), and you can also use the [heroku based demo site](https://www.jotcanvas.com/).

## About

I wrote this one Saturday in 2020 because I needed something to be able to share a whiteboard. Everything I could find required an account or some complicated process to get it working - I just wanted something we could all just browse to.

The front end doesn't have any test. It was from a _stream of consciousness_ coding session. If you want to use it for something more robust, you might want to start there.

Also, it does the whiteboard transfer very naively. It just sends the whole image to everyone and hopes for the best. I was going to add something more interesting, but this basic implementation solved the problem I had so there you go.

The [server](https://gitlab.com/robrohan/jot-server) is a very modified and stripped down version of the server I use for my [game engine](https://meshengine.xyz/) - which is why it might seem a bit overly complex. It uses the webworker in this repo (_socket.worker.ts_) to communicate via websockets to the server in that other repo. All messages are sent in that _IUICommand_ format.
