export const SOCKET_SERVER = process.env.SOCKET_SERVER || 'ws://127.0.0.1:8443/mock/';
export const JOT_VERSION = process.env.JOT_VERSION || 'develop';