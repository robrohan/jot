
export type JotPoint = {
  x: number,
  y: number,
  pressure: number,
}

export type JotLine = {
  width: number,
  color: string,
  cap: 'round' | 'butt' | 'square',
  join: 'round' | 'bevel' | 'miter',
}

export type JotState = {
  drawing: boolean,
  points: Array<{p: JotPoint, l: JotLine}>,
}

///////////////////////////////////////////////////////

export type BusListenerFn = (ev: IUICommand) => void;
export type ModifyListenerFn = (cb: BusListenerFn) => void;

export interface Bus {
  server: string,
  worker: Worker,
  createdCallback: Function,
  listeners: BusListenerFn[],
  subscribe: ModifyListenerFn,
  unsubscribe: ModifyListenerFn,
}

///////////////////////////////////////////////////////

export interface IUICommand {
  type: 'init' | 'disconnect'  // For socket creation / removal
  | 'response'                 // Generic
  | 'boardsync'                // to keep videos in sync
  | 'boardsend'                // send the board image
  | 'join'                     // someone joined the theatre
  | 'chat'                     // chat message
  | 'showing'                  // message to ask about showing
  ;
  payload?: ISocketConnectCommand | any;
}

export interface ISocketConnectCommand {
  url: string;
  token?: string;
}

export interface Showing {
  id: string,
  title: string,
  start: Date,
  viewers: Ticket[],
  reels: Reel[]
  max: number,
}

export interface Reel {
  id: string,
  file: URL,
  // Length in seconds
  duration: number,
  type: 'feature' | 'ad' | 'info'
}

export interface Ticket {
  id: string,
  // Ticket holder's name
  name: string,
  seats: number,
}