import {
  JotLine, JotPoint, JotState,
  Bus, BusListenerFn,
  ISocketConnectCommand, IUICommand
} from './interfaces';
import { JOT_VERSION, SOCKET_SERVER } from './consts';

////////////////////////////////////////////////////////////////

function createFomo(): Bus {
  const fomo = {} as Bus;
  const worker = new Worker('/socket.worker.ts');

  fomo.server = SOCKET_SERVER;
  fomo.worker = worker;
  fomo.listeners = [];
  fomo.subscribe = function (cb: BusListenerFn) {
    if (fomo.listeners.indexOf(cb) >= 0) {
      // already added
    } else {
      fomo.listeners.push(cb);
    }
  };

  fomo.unsubscribe = function (cb: BusListenerFn) {
    const idx = fomo.listeners.indexOf(cb);
    if (idx >= 0) {
      delete fomo.listeners[idx];
    }
  };

  fomo.createdCallback = function () {
    fomo.worker.postMessage({
      type: 'init',
      payload: {
        url: fomo.server
      } as ISocketConnectCommand
    } as IUICommand);

    (fomo.worker as Worker).addEventListener('message', (ev: MessageEvent) => {
      fomo.listeners.forEach(f => {
        if (f) f(ev.data);
      });
    });
  };

  return fomo;
};

////////////////////////////////////////////////////////////////

function pointFromTouch(e: TouchEvent, rect: DOMRect): JotPoint | undefined {
  if (e?.touches && e?.touches[0] && !e?.touches[1]) {
    const first = e.touches[0];
    const p: JotPoint = { x: 0, y: 0, pressure: 0 };

    p.pressure = first['force'] || .01;
    p.x = e.touches[0].pageX - rect.left;
    p.y = e.touches[0].pageY - rect.top;
    return p;
  }

  return undefined;
}

function pointFromMouse(e: MouseEvent, rect: DOMRect): JotPoint | undefined {
  return {
    pressure: .05,
    x: e.pageX - rect.left,
    y: e.pageY - rect.top,
  }
}

function pointFromEvent(e: MouseEvent | TouchEvent, rect: DOMRect): JotPoint | undefined {
  if ((e as any)?.touches) {
    return pointFromTouch(e as TouchEvent, rect);
  }
  return pointFromMouse(e as MouseEvent, rect);
}

function calcLineWidth(width: number, pressure: number): number {
  return ((Math.sin(pressure) * 10) + width);
}

function imageToString(canvas: HTMLCanvasElement) {
  return canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
}

function saveImage(canvas: HTMLCanvasElement) {
  const img = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
  window.location.href = img;
}

////////////////////////////////////////////////////////////////

function createCanvas(width: number, height: number): HTMLCanvasElement {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  return canvas;
}

let selectedColor = '000';
function createLine(): JotLine {
  const dWidth = <HTMLInputElement>document.querySelector('#width');
  const dCap = <HTMLSelectElement>document.querySelector('#cap');
  const dJoin = <HTMLSelectElement>document.querySelector('#join');

  return {
    width: parseFloat(dWidth.value) || .01,
    cap: dCap.options[dCap.selectedIndex].value as any,
    join: dJoin.options[dJoin.selectedIndex].value as any,
    color: `#${selectedColor}`
  }
}

function createColorBar() {
  const bar = <HTMLDivElement>document.querySelector('.colorbar');
  const swatches = [];

  for (let r = 0; r <= 255; r += 64) {
    for (let g = 0; g <= 255; g += 64) {
      for (let b = 0; b <= 255; b += 64) {
        swatches.push(`
          <div class="swatch" 
            style="background-color: rgb(${r}, ${g}, ${b});"
            data-color="${(r.toString(16) + "0").substr(0, 2)}${(g.toString(16) + "0").substr(0, 2)}${(b.toString(16) + "0").substr(0, 2)}"
            ></div>
        `);
      }
    }
  }
  swatches.push(`<div class="swatch" 
      style="background-color: rgb(255,255,255);
      width: 32px;" data-color="FFFFFF"></div>`)
  bar.innerHTML = swatches.join("");
}

////////////////////////////////////////////////////////////////
const state: JotState = {
  drawing: false,
  points: [],
};

function monitor(canvas: HTMLCanvasElement, bus: Bus) {
  const context = canvas.getContext('2d');
  const rect = canvas.getBoundingClientRect();
  if (!context) return;

  let line: JotLine;

  ['touchstart', 'mousedown'].forEach(ev => {
    // @ts-ignore
    canvas.addEventListener(ev, (e: TouchEvent | MouseEvent) => {
      line = createLine();
      let p = pointFromEvent(e, rect);
      state.drawing = true;

      if (p) {
        const lineWidth = calcLineWidth(line.width, p.pressure);
        line.width = lineWidth;
        context.moveTo(p.x, p.y);
        state.points.push({ p, l: line });
      }
    });
  });

  ['touchmove', 'mousemove'].forEach(ev => {
    // @ts-ignore
    canvas.addEventListener(ev, function (e: TouchEvent | MouseEvent) {
      if (!state.drawing) return;
      e.preventDefault();

      let p = pointFromEvent(e, rect);

      if (p) {
        const lineWidth = calcLineWidth(line.width, p.pressure);
        // line.width = lineWidth;
        state.points.push({ p, l: line });

        if (state.points.length >= 3) {
          context.strokeStyle = line.color;
          context.lineCap = line.cap;
          context.lineJoin = line.join;

          const points = state.points;
          const l = points.length - 1;
          const xc = (points[l].p.x + points[l - 1].p.x) * .5;
          const yc = (points[l].p.y + points[l - 1].p.y) * .5;

          context.lineWidth = (points[l - 1].l.width + lineWidth);
          context.quadraticCurveTo(
            points[l - 1].p.x,
            points[l - 1].p.y, xc, yc);
          context.stroke();
          context.beginPath();
          context.moveTo(xc, yc);
        }
      }
    });
  });

  ['touchend', 'touchleave', 'mouseup', 'blur'].forEach(ev => {
    // @ts-ignore
    canvas.addEventListener(ev, function (e: TouchEvent | MouseEvent) {
      state.drawing = false;
      state.points.length = 0;

      // little bit of a debounce
      setTimeout(() => {
        // if they are currently drawing wait for pencils down.
        if (!state.drawing) {
          bus.worker.postMessage({
            type: 'boardsend',
            payload: {
              boardImage: imageToString(canvas)
            }
          });
        }
      }, 1000);
    });
  });
}

////////////////////////////////////////////////////////////////

function hndlConnect(fomo: Bus) {
  return (e: Event) => {
    const name = <HTMLInputElement>document.querySelector('#boardName');
    fomo.worker.postMessage({
      type: 'showing',
      payload: {
        id: name.value,
      }
    } as IUICommand);
  }
}

function hndlSave(canvas: HTMLCanvasElement) {
  return (e: Event) => {
    saveImage(canvas);
  }
}

function hndlSetColor() {
  return (e: Event) => {
    selectedColor = (e?.target as HTMLDivElement)?.getAttribute('data-color') || '000';
  }
}

function hndlDrawGrid(ctx: CanvasRenderingContext2D, w: number, h: number) {
  return (e: Event) => {
    drawGrid(ctx, w, h);
  }
}

function hndlClear(ctx: CanvasRenderingContext2D, w: number, h: number) {
  return (e: Event) => {
    ctx.clearRect(0, 0, w, h);
  }
}

////////////////////////////////////////////////////////////////

function drawGrid(ctx: CanvasRenderingContext2D, w: number, h: number) {
  ctx.strokeStyle = '#f0f0f0';
  ctx.lineCap = 'round';
  ctx.lineJoin = 'round';
  ctx.lineWidth = 1;

  for (let x = 8; x < w; x += 8) {
    if (x % 32 == 0)
      ctx.lineWidth = 2;
    else
      ctx.lineWidth = 1;

    ctx.beginPath();
    ctx.quadraticCurveTo(x, 0, x, h);
    ctx.stroke();
  }

  for (let y = 8; y < h; y += 8) {
    if (y % 32 == 0)
      ctx.lineWidth = 2;
    else
      ctx.lineWidth = 1;

    ctx.beginPath();
    ctx.quadraticCurveTo(0, y, w, y);
    ctx.stroke();
  }

  ctx.beginPath();
  ctx.moveTo(w, h);
}

////////////////////////////////////////////////////////////////

function jotMain(selector: string) {
  console.log(`Starting Jot Canvas ${JOT_VERSION}`);
  const canvasArea = <HTMLDivElement>document.querySelector(selector);

  if (!canvasArea) {
    console.log("Couldn't find canvas area");
    return;
  }

  const w = document.body.clientWidth;              // 640;
  const h = document.body.clientHeight - (48 + 24); // 480;
  const canvas = createCanvas(w, h);
  canvasArea.style.width = w + 'px';
  canvasArea.style.height = h + 'px';
  const ctx = canvas.getContext('2d');

  const img = new Image();

  const fomo = createFomo();
  fomo.createdCallback();
  fomo.subscribe((ev: IUICommand) => {
    switch (ev.type) {

      case 'boardsync': {
        img.src = ev.payload?.boardImage;
        if (ctx)
          img.onload = () => {
            if (!state.drawing) {
              ctx.clearRect(0, 0, img.width, img.height);
              ctx.drawImage(img, 0, 0);
            }
            // TODO: else draw when done drawing...
          }
      } break;

      case 'showing': {
        if (ev.payload?.boardImage) {
          img.src = ev.payload?.boardImage;
          if (ctx)
            img.onload = () => {
              if (!state.drawing) {
                ctx.clearRect(0, 0, img.width, img.height);
                ctx.drawImage(img, 0, 0);
              }
            }
        }
        const btn = <HTMLButtonElement>document.querySelector('#connect');
        btn.disabled = true;
      } break;
    }
  });

  canvasArea.appendChild(canvas);
  monitor(canvas, fomo);

  createColorBar();

  document.querySelector('#connect')?.addEventListener('click', hndlConnect(fomo));
  document.querySelector('#save')?.addEventListener('click', hndlSave(canvas));
  if (ctx) {
    document.querySelector('#clear')?.addEventListener('click', hndlClear(ctx, w, h));
    document.querySelector('#grid')?.addEventListener('click', hndlDrawGrid(ctx, w, h));
  }
  document.querySelectorAll('.swatch').forEach(s => s?.addEventListener('click', hndlSetColor()));

}

window.addEventListener('load', () => { jotMain('.canvasArea') });
