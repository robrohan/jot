import {
  ISocketConnectCommand,
  IUICommand
} from './interfaces';

/////////////////////////////////////////////////////////////////////
const TIMEOUT = 20000;
let socket: WebSocket;
let timerId = 0;

// Init the thread
const init = () => {
  addEventListener('message', onUIMessage);
};

// Init the socket
const socketInit = (command: ISocketConnectCommand) => {
  if (!socket) {
    socket = new WebSocket(command.url);
    socket.onopen = (event: Event) => {
      console.log('Socket open:', event);
      keepAlive(socket);
    };

    socket.onmessage = (event: MessageEvent) => {
      const rtnData = JSON.parse(event.data);
      // console.log('Server says...', rtnData);
      onServerMessage(rtnData);
    };
  }
};

const keepAlive = (webSocket: WebSocket) => {
  if (webSocket?.readyState === WebSocket.OPEN) {
    send('');
  }
  timerId = setTimeout(keepAlive, TIMEOUT);
}

const cancelKeepAlive = () => {
  if (timerId) {
    clearTimeout(timerId);
  }
}

// Destroy the socket
const socketDestory = () => {
  cancelKeepAlive();
  socket.onclose = () => { };
  socket.close();
};

// Listen for messages from the UI
const onUIMessage = (message: MessageEvent) => {
  // console.log('Message from the UI...', message.data);
  switch (message.data.type) {
    case 'init':
      socketInit(message.data.payload);
      break;
    case 'disconnect':
      socketDestory();
      break;
    default:
      // not something we care about, just pass it on
      send(message.data);
  }
};

// Listen to messages from the Sever
// Send messages back to the UI
const onServerMessage = (data: IUICommand) => {
  // Send message to the UI
  postMessage(data);
};

// Kill the thread
const destroy = () => {
  socketDestory();
  close(); // <-- kill ourself
};

// Send messages to the server
const send = (data: any) => {
  if (socket.readyState === WebSocket.OPEN) {
    socket.send(JSON.stringify(data));
  } else {
    console.log('Could not send data. Socket not open');
  }
};

// Startup the thread
init();
